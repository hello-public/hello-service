<?php

namespace App\Http\Controllers;

abstract class Controller
{

    const CODE_SUCCESS = 1;

    const CODE_ERROR_AUTH_FAILED=90001;
    const CODE_ERROR_INTERNAL=99999;

    protected function _returnSuccess(array|string|null $data, string $message = null){
        return response()->json([
            'code' => self::CODE_SUCCESS,
            'success_message' => $message,
            'data' => $data,
        ]);
    }

    protected function _returnError(int $errorCode, string $errorMessage){
        return response()->json([
            'code' => $errorCode,
            'error_message' => $errorMessage,
        ]);
    }

}
