<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class CreateRandomUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:random-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create random user';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $password = 'password'.rand(1, 100);
        $user = User::factory()->create([
            'password' => $password,
        ]);

        $this->info("Random user created successfully! Email: {$user->email}, Password: $password");
    }
}
