<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function info(Request $request)
    {
        $user=$request->user();
        return $this->_returnSuccess(['email'=>$user->email, 'name'=>$user->name,]);
    }

}
